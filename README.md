# README #

Here's the FED Test guys! Had fun doing it. Completed in 2 hours. 

### What is this repository for? ###

* FED Test for Lemonade

### How do I get set up? ###

* Just fork and open index.html locally. 
* I hosted the JSON file on my server to get around the CORS erros on Chrome.

### Contribution guidelines ###

* Would love to hear any suggestions on how I can do better :)

### Who do I talk to? ###

* Probably best to speak to Dee from Vitamin T.
* But if you need to reach me it's Sanny 0481 829 998 or sanny@mokshaweb.com