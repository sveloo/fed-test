// I hosted the file on my server cos I was get heaps of CORS errors trying to load the file locally on my mac.
// Didn't wanna spend time on this and wanted to move on first. My directory has an .htaccess file which bypasses the CORS
// error in Chrome.

$.ajax({
    url: 'http://www.mokshawebserver.com/apps/json/list.json',
    dataType: 'json',
    type: 'get',
    cache: false, // if there more cats are being added later on, we don't want it to cache
    success: function(data){

        // Check if receiving data from JSON
        // console.log(data);

        var catsArray = data.cats;

        // Found out how to do 'Sorting an array of objects in jquery or javascript' on Stack Overflow
        // https://stackoverflow.com/questions/22598334/how-to-render-html-with-jquery-from-an-ajax-call
        // Spent about 15 mins searching for something I could understand and then adapt

        catsArray.sort(function(a, b){
            return a.id - b.id
        })

        // Loop through the sorted array
        $(catsArray).each(function(index, cat){

            // Check that the order is correct
            console.log(cat);

            // A single cat story stored in a var
            var catStory =  '<div class="col s12 m6 cat-story">' +
                '<a href="' + cat.link + '"><img src="' + cat.image + '" class="responsive-img" alt="Image of cat"></a>' +
                '<div class="story-text"><h3><a href="' + cat.link + '">' + cat.heading + '</a></h3><p>' + cat.content + '</p></div></div>'

            // Append the var to the cat-stories div
            $('.cat-stories').append(catStory);
        })

    },
    error: function() {
        console.log('There has been an error connecting to the JSON file.');
    }
})




